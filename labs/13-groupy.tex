%
% These examples are based on the package documentation:
% http://www.ctan.org/tex-archive/macros/latex/contrib/minted
%
\documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[T1]{fontenc}
    \usepackage[utf8]{inputenc}
    \usepackage{lmodern}
    \usepackage{hyperref}
    \usepackage{minted}

    \begin{document}

    \title{Groupy: un servicio de membresía de grupo\footnote{Adaptado al español del material original de Johan Montelius (\url{https://people.kth.se/~johanmon/dse.html})}}
    \author{
        Federico C. Repond \\ {\ttfamily frepond@unq.edu.ar}
        \and
        Esteban Dimitroff Hódi \\ {\ttfamily esteban.dimitroff@unq.edu.ar}
    }
    \maketitle

    \section{Introducción}
    % This is an assignment were you will implement a group membership service that provides atomic multicast. The aim is to have several application layer processes with a coordinated state i.e. they should all perform the same sequence of state changes. A node that wishes to perform a state change must first multicast the change to the group so that all nodes can execute it. Since the multicast layer provides total order, all nodes will be synchronized.
    En este ejercicio vamos a implementar un servicio de membresía de grupo que provee multicast atómico. El objetivo es tener varias capas de procesos de aplicación con un estado coordinado, esto es, todos deben realizar la misma secuencia de cambios de estado. Un nodo que desea hacer un cambio de estado debe primero hacer ``multicast'' del cambio al grupo de forma tal que todos los nodos puedan ejecutarlo. Dado que una capa de multicast provee un orden total, todos los nodos estarán sincronizados.

    % The problem in this assignment is that all nodes need to be synchronized even though nodes may come and go (crash). As you will see it is not as trivial as one might first think.
    El problema en este ejercicio es que todos los nodos estén sincronizados aún cuando los nodos pueden ir y venir (crash). Como veremos no es tan fácil como podríamos pensar.

    \section{Arquitectura}
    % We will implement a group membership service that provides atomic mul- ticast in view synchrony. The architecture consists of a set of nodes where one is the elected leader. All nodes that wish to multicast a message will send the message to the leader and the leader will do a basic multicast to all members of the group. If the leader dies a new leader is elected.
    Vamos a implementar un servicio de membresía de grupo que provee multicast atómico en una vista sincronizada. La arquitectura consiste en un conjunto de nodos donde uno de ellos es electo líder. Todos los nodos que deseen hacer un multicast de un mensaje va a entregar un mensaje al líder y el líder va a hacer un multicast básico a los miembros del grupo. Si el líder muere un nuevo líder es elegido.

    % A new node that wishes to enter the group will contact any node in the group and request to be join the group. The leader will determine when the node is to be included and deliver a new view to the group.
    Si un nuevo nodo desea entrar al grupo va a contactar a cualquiera de los nodos del grupo y pedir ingresar. El líder va a determinar cuando un nodo puede ser incluido y entregar una nueva vista al grupo.

    % Each application layer process will have its own group process that it communicates with. The application layer will send multicast messages to the group process and receive all multicasted messages from it. The application layer must also be prepared to decide if a new node should be allowed to enter the group and also decide the initial state of this node.
    Cada proceso de la capa de aplicación va a tener su propio proceso de grupo con el que se comunica. La capa de aplicación va a entregar mensajes de multicast al proceso de grupo y recibir todos los mensajes de multicast de él. La capa de aplicación debe estar preparada también para decidir si un nuevo nodo va a ser admitido en el grupo y decidir el estado inicial de dicho nodo.

    % Note that we will not deliver any views to the application layer. We could adapt the system so that it reports any view changes but for the application that we are targeting this is not needed. We will keep it as simple as possible and then discuss extensions and how much they would cost.
    Notar que no vamos a entregar ninguna vista a la capa de aplicación. Podemos adatar el sistema para que reporte cualquier cambio en la vista, pero para la aplicación que nos interesa esto no es necesario. Vamos a mantener la aplicación los más simple posible y discutir las extensiones posibles y cual sería su costo.

    \subsection{Vista Sincronizada}
    % Each node in the group should be able to multicast messages to the members of the group. The communication is divided into views and messages will be said to be delivered in a view. For all messages in a view we will guarantee the following:
    Cada nodo del grupo debe ser capaz de hacer multicast de los mensajes a los miembros del grupo. La comunicación está dividida en vistas y los mensajes serán entregados en una vista. Todos los mensajes en una vista van a garantizar los siguiente:

    \begin{itemize}
    % in FIFO order: in the order that they were sent by a node
      \item en orden FIFO: en el orden que fueron entregados por un nodo
    % in total order: all nodes see the same sequence
      \item en orden total: todos los nodos ven la misma secuencia
    % reliably: if a correct node delivers a message, all correct nodes deliver the message
      \item confiabilidad: si un nodo ``correcto'' entrega un mensaje, todos los nodos entregan el mensaje
    \end{itemize}

    % The last statement seems to be a bit weak, what do we mean by a correct node? A node will fail only by crashing and will then never be heard from again. A correct node is a node that does not fail during a view i.e. it survives to install the next view.
    El último punto parece un poco débil, qué queremos decir con un nodo correcto? Un nodo va a fallar solo si muere y nunca volvemos a escuchar de el nuevamente. Un nodo correcto es un nodo que no falla durante un vista, esto es, sobrevive a instalar la siguiente vista.

    % It will not be guaranteed that sent messages are delivered, we will use asynchronous sending without acknowledgment and if we have a failing leader a sent message might disappear.
    No va a asegurar que los mensajes enviados son entregados, vamos a usar envío asincrónico sin chequeo y si un líder falla un mensaje puede perderse.

    \subsection{El Líder}
    % A node will either play the role of a leader (let’s hope there is only one) or a slave. All slaves will forward messages to the leader and the leader will tag each message with a sequence number and multicast it to all nodes. The leader can also accept a message directly from its own master i.e. the application layer. The application layer is unaware of whether its group process is acting as a leader or a slave.
    Un nodo puede jugar el rol de un líder (esperemos que sea el único!) o esclavo. Todos los esclavos van a reenviar los mensajes al líder y el líder va a tagear cada mensaje con un número de secuencia y hacer multicast a todos los nodos. El líder puede aceptar también un mensaje de su propio master, por ejemplo, de la capa de aplicación. La capa de aplicación no sabe si su proceso de grupo está actuando como líder o esclavo.

    \subsection{Un Esclavo}
    % A slave will receive messages from its application layer process and forward them to the leader. It will also receive messages from the leader and forward them to the application layer. If nodes would not fail this would be the easiest job in the world but since we must be able to act if the leader dies we need to do some bookkeeping.
    Un esclavo va recibir mensajes desde su capa de aplicación y reenviarlos al líder. También va a recibir mensajes del líder y reenviarlos a la capa de aplicación. Si los nodos no fallan esto va a ser la tarea más fácil del mundo, pero dado que debemos ser capaces de actuar como líder si el líder muere necesitamos hacer algo de mantenimiento.

    % In our first version of the implementation, we will not deal with failures but only with adding new nodes to the system. This is complicated enough to start with.
    En nuestra primera versión de la implementación, no vamos a tratar con fallas solo con el agregado de nuevos nodos al sistema. Esto es suficientemente complicado para empezar.

    \subsection{La Elección}
    % The election procedure is very simple. All slaves have the same list of peers and they all elect the first node in the list as the leader. A slave that detects that it is the first node will of course adopt the role as leader. The leader will have to resend the last message that it received and the slaves will have to monitor the new leader.
    La elección es un procedimiento simple. Todos los esclavos tienen la misma lista de pares y ellos eligen el primer nodo de la lista como líder. Un esclavo que detecta que es el primer nodo va a adoptar el rol de líder. El líder va a tener que reenviar el último mensaje que recibió y los esclavos tienen que monitorear el nuevo líder.

    \subsection{La Capa de Aplicación}
    % An application process will create a group process and contact any other application process it knows of. It will request to join the group providing the process identifier of its group process. It will then wait for a view delivery, containing the peer processes in the group.
    Un proceso de aplicación va a crear un proceso de grupo y contactar cualquier otro proceso de aplicación que conozca. Va pedir unirse al grupo proveyendo el identificador de proceso de su proceso de grupo. Entonces va a esperar por la entrega de una vista, conteniendo los procesos pares en el grupo.

    % There is no guarantee that the request is delivered to the leader or the leader could be dead and we have not detected this yet. The requesting application process is however not told about this so we can not do anything but wait and hope for the best. We will use a timeout and if we have not been invited we just abort the attempt.
    No hay garantía de que el pedido se entregado al líder o que el líder murió y todavía no lo hemos detectado aún. El proceso de aplicación que envía el pedido sin embargo no recibe notificaciones de que esto sucede por lo tanto no podemos hacer nada más que esperar lo mejor. Vamos a usar un timeout y si no fuimos invitados vamos a cancelar el pedido.

    % Once added to the group the application process has the problem of obtaining the correct state (the color). It does this by using the atomic multicast layer in a clever way. It sends a request to obtain the state and waits for this message to be delivered to itself. It now knows that the other processes sees this message and respond by sending the state, also using the multicast layer.
    Una vez que es agregado al grupo el proceso de aplicación tiene el problema de obtener el estado correcto (el color). Esto se hace usando multicast atómico convenientemente. Envía un pedido para obtener el estado y espera que se le entregue el mensaje a si mismo. Sabe ahora que otros procesos ven los mensajes y responden enviando el estado, también usando la capa de multicast.

    % The state message might however not be the first message that is deliv- ered. We might have other state changes in the pipeline. Once the state is received these state changes must of course be applied to the state before the process is up and running. The implementation uses the implicit deferral of Erlang and simply let any state change messages remain in the message queue and chooses to handle the state message first before the state change messages.
    El mensaje de estado podría sin embargo no ser el primer mensaje entregado. Podríamos tener otros cambio de estado en el pipeline. Una vez que el estado es recibido estos cambios de estado deben por supuesto ser aplicados al estado antes de que el proceso este listo. La implementación usa el aplazamiento explícito de Erlang y simplemente deja que cualquier mensaje de cambio de estado se mantenga en la cola y elige manejar el mensaje de estado primero antes de procesar los cambios.

    \section{La Primer Implementación}
    % Our first version, called gms1, will only handle starting of a single node and the adding of more nodes. Failures will not be handled so some of the states that we need to keep track of is not described. We will then extend this implementation to handle failures.
    Nuestra primera versión, llamada \texttt{gms1}, va a manejar iniciar un solo nodo y y el agregado de más nodos. Las fallas no van a ser manejadas así que algunos de los estados que debemos mantener no los vamos a describir aún. Vamos a extender luego esta implementación para manejar fallas.

    % The group process will when started be a slave but might in the future become a leader. The first process that is started will however become a leader directly.
    El proceso de grupo va a ser arrancado como un esclavo pero podría en el futuro convertirse en líder. El primer proceso que es arrancado sin embargo se va a convertir en líder directamente.

    \subsection{El Proceso Líder}
    %The leader keeps the following state:
    El líder va a mantener el siguiente estado:

    \begin{itemize}
    % Id: a unique name, of the node, only used for debugging
      \item \texttt{Id}: un nombre único, solo usado para debugging
    % Master: the process identifier of the application layer
      \item \texttt{Master}: el identificador de proceso de la capa de aplicación
    % Slaves: an ordered list of the process identifiers of all slaves in the group
      \item \texttt{Slaves}: una lista ordenada de los identificadores de proceso de todos los esclavos del grupo
    % Group: a list of all application layer processes in the group
      \item \texttt{Group}: una lista con todos los procesos de la capa de aplicación del grupo
    \end{itemize}

    % The list of slaves is ordered based on when they were admitted to the group. We will use this order in the election procedure.
    La lista de todos los esclavos está ordenada basada en cuando fueron admitidos en el grupo. Vamos a usar este orden en el procedimiento de elección.

    % The leader should be able to handle the following messages:
    El líder deberá ser capaz de manejar los siguientes mensajes:

    \begin{itemize}
    % {mcast, Msg}: a message either from its own master or from a peer node. A message {msg, Msg} is multicasted to all peers and a message Msg is sent to the application layer.
      \item \texttt{{mcast, Msg}}: un mensaje es de su propio master o de algún par del nodo. Se debe hacer multicast un mensaje \texttt{{mcast, Msg}} a todos sus pares y el mensaje \texttt{Msg} enviado a la capa de aplicación.
    % {join, Wrk, Peer}: a message, from a peer or the master, that is a request from a node to join the group. The message contains both the process identifier of the application layer, Wrk, and the process identifier of its group process.
      \item \texttt{{join, Wrk, Peer}}: un mensaje de un par o el master, que es un pedido de un nodo para unirse al grupo. El mensaje contienen tanto el identificador del proceso de la capa de aplicación, \texttt{Wrk}, como el identificador del proceso de su proceso de grupo.
    \end{itemize}

    % The state of a leader is implemented by the following procedure. We use a function bcast/3 that will send a message to each of the processes in a list.
    El estado de un líder es implementado de la siguiente función. Usamos una función \texttt{bcast/3} que va a enviar un mensaje a cada uno de los procesos en una lista.

    \begin{minted}{erlang}
    leader(Id, Master, Slaves, Group) ->
        receive
            {mcast, Msg} ->
                bcast(Id, {msg, Msg}, Slaves),
                Master ! Msg,
                leader(Id, Master, Slaves, Group);
            {join, Wrk, Peer} ->
                Slaves2 = lists:append(Slaves, [Peer]),
                Group2 = lists:append(Group, [Wrk]),
                bcast(Id, {view, [self()|Slaves2], Group2}, Slaves2),
                Master ! {view, Group2},
                leader(Id, Master, Slaves2, Group2);
            stop -> ok
        end.
    \end{minted}

    % Notice that we add the new node at the end of the list of peers. This is important, we want the new node to be the last one to see the view message that we send out. More on this later when we look at failing nodes.
    Notar que agregamos un nuevo nodo al final de la lista de pares. Esto es importante, queremos que un nuevo nodo sea el último en ver un mensaje de las vista que enviamos. Veremos más sobre esto más adelante cuando veamos nodos que fallan.

    \subsection{Un Esclavo}
    % A slave has an even simpler job, it will not make any complicated decisions. It is simply forwarding messages from its master to the leader and vice verse. The state of a slave is exactly the same as for the leader with the only exception that the slaves keep explicit track of the leader.
    Un esclavo tiene una tarea aún más fácil, no va tomar ninguna decisión complicada. Solo va a reenviar mensajes del master al líder y viceversa. El estado de un esclavo es exactamente el estado del líder con la sola excepción que los esclavos mantienen registro explícito del líder.

    % The messages from the master are the following:
    Los mensajes del master son los siguiente:

    \begin{itemize}
    % {mcast, Msg}: a request from its master to multicast a message, the message is forwarded to the leader.
      \item \texttt{{mcast, Msg}}: un pedido de su master para hacer multicast de un mensaje, el mensaje es reenviado al líder.
    % {join, Wrk, Peer}: a request from the master to allow a new node to join the group, the message is forwarded to the leader.
      \item \texttt{{join, Wrk, Peer}}: un request de un master para permitir que un nuevo nodo se una al grupo, el mensaje es reenviado al líder.
    % {msg, Msg}: a multicasted message from the leader. A message Msg is sent to the master.
      \item \texttt{{msg, Msg}}: un mensaje que se hizo multicast desde un líder. Un mensaje \texttt{Msg} es enviado al master.
    % {view, Peers, Group}: a multicasted view from the leader. A view is delivered to the master process.
      \item \texttt{{view, Peers, Group}}: una vista que se hizo multicast desde el líder. La vista es entregada al proceso master.
    \end{itemize}

    % This is the implementation of the slave:
    Esta es la implementación del esclavo:

    \begin{minted}{erlang}
    slave(Id, Master, Leader, Slaves, Group) ->
        receive
            {mcast, Msg} ->
                Leader ! {mcast, Msg},
                slave(Id, Master, Leader, Slaves, Group);
            {join, Wrk, Peer} ->
                Leader ! {join, Wrk, Peer},
                slave(Id, Master, Leader, Slaves, Group);
            {msg, Msg} ->
                Master ! Msg,
                slave(Id, Master, Leader, Slaves, Group);
            {view, [Leader|Slaves2], Group2} ->
                Master ! {view, Group2}
                slave(Id, Master, Leader, Slaves2, Group2);
            stop ->
                ok
        end.
    \end{minted}

    %Since we will not yet deal with failure there is no transition between being a slave and becoming a leader. We will add this later but first let us have this thing up and running.
    Dado que no vamos a lidiar con fallas no hay transición de esclavo a líder. Vamos a agregar esto luego pero primero vamos a tratar de tener algo andando.

    \subsection{Inicialización}
    % Initializing a process that is the first node in a group is simple. The only thing we need to do is to give it an empty list of peers and let it know that its master is the only node in the group. Since it is the only node in the group it will of course be the leader of the group.
    Inicializar un proceso que es el primer nodo en un grupo es simple. Lo único que necesitamos hacer es darle una lista vacía de pares y hacerles saber que su master es es el único nodo del grupo. Dado que es el único nodo en el grupo por supuesto va a ser el líder del grupo.

    \begin{minted}{erlang}
    start(Id) ->
        Self = self(),
        {ok, spawn_link(fun()-> init(Id, Self) end)}.

    init(Id, Master) ->
        leader(Id, Master, [], [Master]).
    \end{minted}

    % Starting a node that should join an existing group is only slightly more problematic. We need to send a {join, Master, self()} message to a node in the group and wait for an invitation. The invitation is delivered as a view message containing everything we need to know. The initial state is of course as a slave.
    Iniciar un nodo que debe unirse a un grupo existente es un poco más complicado. Necesitamos enviar un mensaje \texttt{{join, Master, self()}} a un nodo en el grupo y esperar la invitación. La invitación es entregada como un mensaje de una vista conteniendo todo lo que conocemos. El estado inicial es por supuesto como esclavo.

    \begin{minted}{erlang}
    start(Id, Grp) ->
        Self = self(),
        {ok, spawn_link(fun()-> init(Id, Grp, Self) end)}.

    init(Id, Grp, Master) ->
        Self = self(),
        Grp ! {join, Master, Self},
        receive
            {view, [Leader|Slaves], Group} ->
                Master ! {view, Group},
                slave(Id, Master, Leader, Slaves, Group)
        end.
    \end{minted}

    \subsection{El Proceso Aplicación}
    % To do some experiment we create worker that uses a gui to describe its state. Make sure that you can create a group and add some peers.
    Para hacer alguna prueba creamos un worker que use una GUI para describir su estado. Asegurarse de que podemos crear un grupo y agregar algunos pares.

    \section{Manejando Errores}
    % We will build up our fault tolerance gradually. First we will make sure that we detect crashes, then to make sure that a new leader is elected an then make sure that the layer preserves the properties of the atomic multicast. Keep gms1 as a reference and call the adapted module gms2.
    Vamos a construir nuestro esquema de tolerancia a fallas en forma gradual. Primero vamos a asegurarnos de detectar las fallas, luego nos vamos a asegurar que un líder es electo y por último asegurarnos que la capa preserva la propiedad de multicast atómico. Mantener \texttt{gms1} como referencia y llamemos al módulo adaptado \texttt{gms2}.
    \subsection{Detectores de Fallas}
    % We will use the Erlang built in support to detect and report that processes have crashed. A process can monitor another node an if that nodes dies a message will be received. For now we will assume that the monitors are perfect i.e. they will eventually report the crash of a node and they will never report the death of a node that has not died.
    Vamos a usar el soporte de Erlang para detectar y reportar que los procesos murieron. Un proceso puede monitorear otro nodo y si ese nodo muere va a recibir un mensaje. Por ahora vamos a asumir que los monitores son perfectos, esto es, eventualmente va a a recibir el reporte de falla de de un nodo y nunca van a reportar la muerte de un nodo que no lo haya hecho.

    % We will also assume that the message that inform a process about a death of a process is the last message that it will see from the node. The message will thus be received in FIFO order as any regular message.
    También vamos a asumir que los mensajes que informan a un proceso de la muerte de otro es el último mensaje que vemos de un nodo. El mensaje entonces es recibido en orden FIFO como cualquier mensaje común.

    % The question we first need to answer is, who should monitor who? In our architecture we need not report new views when a slave dies and there is nothing to prevent a dead slave to be part of a view so we will keep things simple; the only node that will be monitored is the leader. A slave that detects that a leader has died will move to an election state.
    La pregunta que primero debemos responder es, quién debe monitorear a quién? En nuestra arquitectura no necesitamos reportar nuevas vistas cuando un esclavo muere y no hay nada que prevenga que un esclavo muerto sea parte de una vista, así que vamos a mantener las cosas simples; el único nodo que va a ser monitoreado es el líder. Un esclavo que detecta que un líder murió cambiará a un estado de elección.

    % This is implemented by first adding a call to erlang:monitor/2 in the initialization of the slave:
    Esto se implementa agregando primero la llamada \texttt{erlang:monitor/2} en la inicialización del esclavo:

    \begin{minted}{erlang}
        erlang:monitor(process, Leader)
    \end{minted}

    % and a new clause in the state of the slave:
    y una nueva cláusula al estado del esclavo:

    \begin{minted}{erlang}
        {’DOWN’, _Ref, process, Leader, _Reason} ->
            election(Id, Master, Slaves, Group);
    \end{minted}

    % In the election state the process will select the first node in its lists of peers and elect this as the leader. It could of course be that the process finds itself being the first node and it will thus become the leader of the group.
    En el estado de elección el proceso va a seleccionar el primer nodo de su lista de pares y seleccionarlo como líder. Podría pasar por supuesto que el propio proceso sea el primer nodo y en tal caso convertirse en líder del grupo.

    \begin{minted}{erlang}
    election(Id, Master, Slaves, [_|Group]) ->
        Self = self(),
        case Slaves of
            [Self|Rest] ->
                bcast(Id, {view, Slaves, Group}, Rest),
                Master ! {view, Group},
                leader(Id, Master, Rest, Group);
            [Leader|Rest] ->
                erlang:monitor(process, Leader),
                slave(Id, Master, Leader, Rest, Group)
        end.
    \end{minted}

    % One thing that we have to pay attention to is what we should do if, as a slave, receive the view message from the new leader before we have noticed that the old leader is dead. Should we refuse to handle view messages unless we have seen the Down message from the leader or should we happily receive accept the new view and then ignore trailing Down messages.
    Algo que debemos prestar atención es que debemos hacer si, como esclavo, recibimos un \emph{mensaje de vista} de un nuevo líder antes que hayamos detectado que el viejo líder murió. Deberíamos negarnos a manejar mensajes de vista a menos que hayamos visto un \emph{mensaje Down} del líder? o simplemente deberíamos aceptar el mensaje del nuevo líder e ignorar el \emph{mensaje Down}?

    % Since the leader can crash it could be that a node that wants to join the group will never receive a reply. The message could be forwarded to a dead leader and the joining node is never informed of the fact that its request was lost. We simply add a timeout when waiting for an invitation to join the group.
    Dado que el líder puede morir podría ser que un nodo que se quiere unir nunca reciba una respuesta. El mensaje puede ser reenviado a un líder muerto y el nodo queriendo unirse nunca va a ser informado del hecho que su pedido se perdió. Para esto simplemente agregamos un timeout cuando esperamos una invitación para unirnos al grupo.

    \begin{minted}{erlang}
        after ?timeout ->
            Master ! {error, "no reply from leader"}
    \end{minted}

    % That is it we can now both add new nodes to the system and survive even if nodes crash. That was not that hard was it? Do some experiments to see that it works and then ship the product.
    Ahora podemos agregar nodos nuevos al sistema y sobrevivir aún cuando los nodos mueren. No fue tan difícil, no? Hacer algunas pruebas para ver que esto funciona antes de que podamos entregar el producto.

    \subsection{Mensajes Perdidos}
    % Is seams to be too easy and unfortunately it is. To show that it is not working we can change the bcast/3 procedure and introduce a random crash. We define a constant arghh that defines the risk of crashing. A value of 100 means that a process will crash in average once in a hundred attempts. The definition of bcast/3 now looks like this:

    Parece que es demasiado fácil la solución y desafortunadamente los es. Para mostrar que no funciona  podemos cambiar la función \texttt{bcast/3} e introducir caídas aleatorias. Definimos una constante \texttt{arrghh} que define el riesgo de caída. Un valor de \texttt{100} significa que el proceso va a caerse en promedio una vez cada 100 intentos. La definición de \texttt{bcast/3} ahora se ve de la siguiente forma:

    \begin{minted}{erlang}
    bcast(Id, Msg, Nodes) ->
        lists:foreach(fun(Node) -> Node ! Msg, crash(Id) end, Nodes).
    crash(Id) ->
        case random:uniform(?arghh) of
            ?arghh ->
                io:format("leader ~w: crash~n", [Id]),
                exit(no_luck);
            _ -> ok
        end.
    \end{minted}

    % Run some experiments and see if you can have the state of the workers become out of sync. What is happening?
    Correr algunas pruebas y ver si tenemos estado en los nodos que quedan de-sincronizados. Qué está sucediendo?

    \subsection{Multicast Confiable}
    % To remedy the problem we could replace the basic multicaster with a reliable multicaster. A process that would forward all messages before delivering them to the higher layer. Using a vanilla reliable multicaster would however be very costly, we could try a smarter solution.
    Para remediar el problema podemos remplazar el multicaster básico con uno confiable. Un proceso va a reenviar todos los mensajes antes de pasarlos a una capa superior. Usar un multicaster confiable básico podría ser muy costoso, vamos a intentar una solución más inteligente.

    % Assume that we keep a copy of the last message that we have seen from the leader. If we detect the death of the leader it could be that it died during the basic multicast procedure and that some nodes have not seen the message. We will now make an assumption that we will discuss later:
    Asumamos que mantenemos una copia del último mensaje que vimos de un líder. Si detectamos la muerte del líder puede que haya sucedido durante el procedimiento de multicast básico y algunos nodos no hayan visto el mensaje. Vamos a asumir algo que vamos a discutir más adelante:

    % Messages are reliably delivered and thus,
    % if the leader sends a message to A and then B, and B receives the message, then also A will receive the message.
    \begin{itemize}
      \item Los mensajes son confiables entonces,
      \item si un líder envía un mensaje a A y entonces B, y B recibe el mensaje, entonces A va a recibir el mensaje.
    \end{itemize}

    % The leader is sending messages to the peers in the order that they occur in the list of peers. If anyone receives a message then the first peer in the list receives the message. This means that only the next leader needs to resend the message.
    El líder envía mensajes a los pares en el orden que aparecen en la lista de pares. Si alguno recibe un mensaje entonces el primer par en la lista recibe el mensaje. Esto significa que solo el siguiente al líder necesita reenviar el mensaje.

    % This will of course introduce the possibilities of doublets of messages being received. In order to detect this we will number all messages and only deliver new messages to the application layer.
    Esto por supuesto va a introducir la posibilidad de que se dupliquen mensajes recibidos. Para detectar esto vamos a numerar todos los mensajes y solo entregar nuevos mensajes a la capa de aplicación.

    % Lets go through the changes that we need to make and create a new module gms3 that implements these changes.
    Vamos a ver los cambios que necesitamos para hacer el nuevo módulo \texttt{gsm3} que implementa estos cambios.

    % slave(Id, Master, Leader, N, Last, Slaves, Group): the slave procedure is extended with two arguments: N and Last. N is the expected sequence number of the next message and Last is a copy of the last message (a regular message or a view) received from the leader.
    % election(Id, Master, N, Last, Slaves, Group): the election pro- cedure is extended with the same two arguments.
    % leader(Id, Master, N, Slaves): the leader procedure is extended with the the argument N, the sequence number of the next message (regular message or view) to be sent.

    \begin{itemize}
      \item \texttt{slave(Id, Master, Leader, N, Last, Slaves, Group)}: el procedimiento del esclavo es extendido con 2 argumentos: \texttt{N} y \texttt{Last}. \texttt{N} es el número de secuencia esperado del siguiente mensaje y \texttt{Last} es una copia del último mensaje (un mensaje común o de vista) recibido del líder.
      \item \texttt{election(Id, Master, N, Last, Slaves, Group)}: el mecanismo de elección se extiende con los mismos 2 argumentos.
      \item \texttt{leader(Id, Master, N, Slaves)}: el líder es extendido con el argumento \texttt{N}, el número de secuencia del siguiente mensaje (común o vista) a ser enviado.
    \end{itemize}

    % The messages are also changed and will now contain the sequence number.
    Los mensajes también hay que cambiarlos para que ahora tengan el número de secuencia.

    % {msg, N, Msg}: a regular message with a sequence number.
    % {view, N, Peers, Group}: a view message with a sequence number.

    \begin{itemize}
      \item \texttt{{msg, N, Msg}}: un mensaje común con el número de secuencia.
      \item \texttt{{view, N, Peers, Group}}: una vista con un número de secuencia.
    \end{itemize}

    % We must also add clauses to the slave to accept and ignore duplicate messages. If we do not remove these from the message queue they will add up and after a year generate a very hard to handle trouble report.
    También debemos agregar cláusulas al esclavo para aceptar e ignorar mensajes duplicados. Si no removemos estos mensajes de la cola de mensajes se van a ir encolando y generar problemas.

    % When discarding messages we discard messages we only want to discard messages that we have seen i.e. messages with a sequence number less than N . We can do this by using the when construction. For example:
    Cuando descartamos mensajes solo descartamos aquellos que ya hemos visto, esto es, mensajes con número de secuencia menor que \texttt{N}. Hacemos esto usando la construcción \texttt{when}. Por ejemplo:

    \begin{minted}{erlang}
    {msg, I, _} when I < N ->
        slave(Id, Master, Leader, N, Last, Slaves, Group);
    \end{minted}

    % You might wonder how a message possibly could arrive early but there is a a small window where this could actually happen.
    Nos estaremos preguntando cómo un mensaje podría llegar temprano, pero hay una pequeña ventana donde esto podría efectivamente pasar.

    % The crucial part is then in the election procedure where the elected leader will forward the last received message to all peers in the group. Hopefully this will be enough to keep slaves synchronized.
    La parte crítica es en el procedimiento de elección cuando el líder elegido va a reenviar el último mensaje recibido a todos su pares en el grupo. Con suerte esto va a ser suficiente para mantener los esclavos sincronizados.

    \begin{minted}{erlang}
      bcast(Id, Last, Rest),
    \end{minted}

    % This completes the transition and gms3 should be ready for release.
    Esto completa la transición y \texttt{gms3} debería estar listo para producción.

    \subsection{Algunos Experimentos}
    % Run some experiments and create a large group spanning several computers. Can we keep a group rolling by adding more nodes as existing nodes die?
    Ejecutar algunos experimentos y crear un grupo grande que se extienda a varias computadoras. Hay sin embargo un tema que debemos mencionar an esto es que nuestra implementación no funciona. Bueno, algunas cosas funcionan dependiendo de que garantice el entorno Erlang y cuan fuerte son nuestros requerimientos.
    % Assuming all test went well we’re ready to ship the product. There is however one thing we need to mention and that is that our implementa- tion does not work. Well, it sort of works depending on what the Erlang environment guarantees and how strong our requirements are.

    \section{Qué Podría Salir Mal?}
    % The first thing we have to realize is what guarantees the Erlang system actually gives on message sending. The specifications only guarantee that messages are delivered in FIFO order, not that they actually do arrive. We have built our system relying on reliable delivery of messages, something that is not guaranteed.
    Lo primero que debemos darnos cuentas es que garantiza Erlang en el envío de mensajes. La especificación garantiza solo que os mensajes son entregados en orden FIFO, no que necesariamente lleguen. Nosotros construimos nuestro sistema basados en la entrega confiable de mensajes, algo que no está garantizado.

    % How would we have to change the implementation to handle the possibly lost messages? How would this impact performance?
    Cómo deberíamos cambiar nuestra implementación para manejar la posibilidad de que se pierda un mensaje? Cuál sería el impacto de esto en la performance?

    % The second reason why things will not work is that we rely on that the Erlang failure detector is perfect i.e. that it will never suspect any correct node for having crashed. Is this really the case? Can we adapt the system so that it will behave correctly if it does make progress, even though it might not always make progress?
    La segunda razón por la cual las cosas no funcionan es que nos basamos que la detección de fallas de Erlang es perfecto, esto es, que nunca va a sospechar que un nodo correcto ha muerto. Es esto siempre así? Podemos adaptar el sistema para que se comporte correctamente si hay progreso, aún cuando podría no siempre tener progreso?

    % The third reason why things do not work is that we could have a situation where one incorrect node delivers a message that will not be delivered by any correct node. This could happen even if we had reliable send operations and perfect failure detectors. How could this happen and how likely is it that it does? What would a solution look like?
    La tercer razón de por qué las cosas no funcionan es que podemos tener una situación donde un nodo incorrecto entrega un mensaje que no fue entregado por ningún nodo correcto. Esto puede suceder aún si tenemos envío confiable y detección de fallas perfecta. Cómo podría pasar esto y cuán seguido Cómo sería una solución?
    \end{document}
