# Groupy: un servicio de membresía de grupo

Introducción
============

En este ejercicio vamos a implementar un servicio de membresía de grupo
que provee multicast atómico. El objetivo es tener varias capas de
procesos de aplicación con un estado coordinado, esto es, todos deben
realizar la misma secuencia de cambios de estado. Un nodo que desea
hacer un cambio de estado debe primero hacer "multicast" del cambio al
grupo de forma tal que todos los nodos puedan ejecutarlo. Dado que una
capa de multicast provee un orden total, todos los nodos estarán
sincronizados.

El problema en este ejercicio es que todos los nodos estén sincronizados
aún cuando los nodos pueden ir y venir (crash). Como veremos no es tan
fácil como podríamos pensar.

Arquitectura
============

Vamos a implementar un servicio de membresía de grupo que provee
multicast atómico en una vista sincronizada. La arquitectura consiste en
un conjunto de nodos donde uno de ellos es electo líder. Todos los nodos
que deseen hacer un multicast de un mensaje va a entregar un mensaje al
líder y el líder va a hacer un multicast básico a los miembros del
grupo. Si el líder muere un nuevo líder es elegido.

Si un nuevo nodo desea entrar al grupo va a contactar a cualquiera de
los nodos del grupo y pedir ingresar. El líder va a determinar cuando un
nodo puede ser incluido y entregar una nueva vista al grupo.

Cada proceso de la capa de aplicación va a tener su propio proceso de
grupo con el que se comunica. La capa de aplicación va a entregar
mensajes de multicast al proceso de grupo y recibir todos los mensajes
de multicast de él. La capa de aplicación debe estar preparada también
para decidir si un nuevo nodo va a ser admitido en el grupo y decidir el
estado inicial de dicho nodo.

Notar que no vamos a entregar ninguna vista a la capa de aplicación.
Podemos adatar el sistema para que reporte cualquier cambio en la vista,
pero para la aplicación que nos interesa esto no es necesario. Vamos a
mantener la aplicación los más simple posible y discutir las extensiones
posibles y cual sería su costo.

Vista Sincronizada
------------------

Cada nodo del grupo debe ser capaz de hacer multicast de los mensajes a
los miembros del grupo. La comunicación está dividida en vistas y los
mensajes serán entregados en una vista. Todos los mensajes en una vista
van a garantizar los siguiente:

-   en orden FIFO: en el orden que fueron entregados por un nodo

-   en orden total: todos los nodos ven la misma secuencia

-   confiabilidad: si un nodo "correcto" entrega un mensaje, todos los
    nodos entregan el mensaje

El último punto parece un poco débil, qué queremos decir con un nodo
correcto? Un nodo va a fallar solo si muere y nunca volvemos a escuchar
de el nuevamente. Un nodo correcto es un nodo que no falla durante un
vista, esto es, sobrevive a instalar la siguiente vista.

No va a asegurar que los mensajes enviados son entregados, vamos a usar
envío asincrónico sin chequeo y si un líder falla un mensaje puede
perderse.

El Líder
--------

Un nodo puede jugar el rol de un líder (esperemos que sea el único!) o
esclavo. Todos los esclavos van a reenviar los mensajes al líder y el
líder va a tagear cada mensaje con un número de secuencia y hacer
multicast a todos los nodos. El líder puede aceptar también un mensaje
de su propio master, por ejemplo, de la capa de aplicación. La capa de
aplicación no sabe si su proceso de grupo está actuando como líder o
esclavo.

Un Esclavo
----------

Un esclavo va recibir mensajes desde su capa de aplicación y reenviarlos
al líder. También va a recibir mensajes del líder y reenviarlos a la
capa de aplicación. Si los nodos no fallan esto va a ser la tarea más
fácil del mundo, pero dado que debemos ser capaces de actuar como líder
si el líder muere necesitamos hacer algo de mantenimiento.

En nuestra primera versión de la implementación, no vamos a tratar con
fallas solo con el agregado de nuevos nodos al sistema. Esto es
suficientemente complicado para empezar.

La Elección
-----------

La elección es un procedimiento simple. Todos los esclavos tienen la
misma lista de pares y ellos eligen el primer nodo de la lista como
líder. Un esclavo que detecta que es el primer nodo va a adoptar el rol
de líder. El líder va a tener que reenviar el último mensaje que recibió
y los esclavos tienen que monitorear el nuevo líder.

La Capa de Aplicación
---------------------

Un proceso de aplicación va a crear un proceso de grupo y contactar
cualquier otro proceso de aplicación que conozca. Va pedir unirse al
grupo proveyendo el identificador de proceso de su proceso de grupo.
Entonces va a esperar por la entrega de una vista, conteniendo los
procesos pares en el grupo.

No hay garantía de que el pedido se entregado al líder o que el líder
murió y todavía no lo hemos detectado aún. El proceso de aplicación que
envía el pedido sin embargo no recibe notificaciones de que esto sucede
por lo tanto no podemos hacer nada más que esperar lo mejor. Vamos a
usar un timeout y si no fuimos invitados vamos a cancelar el pedido.

Una vez que es agregado al grupo el proceso de aplicación tiene el
problema de obtener el estado correcto (el color). Esto se hace usando
multicast atómico convenientemente. Envía un pedido para obtener el
estado y espera que se le entregue el mensaje a si mismo. Sabe ahora que
otros procesos ven los mensajes y responden enviando el estado, también
usando la capa de multicast.

El mensaje de estado podría sin embargo no ser el primer mensaje
entregado. Podríamos tener otros cambio de estado en el pipeline. Una
vez que el estado es recibido estos cambios de estado deben por supuesto
ser aplicados al estado antes de que el proceso este listo. La
implementación usa el aplazamiento explícito de Erlang y simplemente
deja que cualquier mensaje de cambio de estado se mantenga en la cola y
elige manejar el mensaje de estado primero antes de procesar los
cambios.

La Primer Implementación
========================

Nuestra primera versión, llamada `gms1`, va a manejar iniciar un solo
nodo y el agregado de más nodos. Las fallas no van a ser manejadas así
que algunos de los estados que debemos mantener no los vamos a describir
aún. Vamos a extender luego esta implementación para manejar fallas.

El proceso de grupo va a ser arrancado como un esclavo pero podría en el
futuro convertirse en líder. El primer proceso que es arrancado sin
embargo se va a convertir en líder directamente.

El Proceso Líder
----------------

El líder va a mantener el siguiente estado:

-   `Id`: un nombre único, solo usado para debugging

-   `Master`: el identificador de proceso de la capa de aplicación

-   `Slaves`: una lista ordenada de los identificadores de proceso de
    todos los esclavos del grupo

-   `Group`: una lista con todos los procesos de la capa de aplicación
    del grupo

La lista de todos los esclavos está ordenada basada en cuando fueron
admitidos en el grupo. Vamos a usar este orden en el procedimiento de
elección.

El líder deberá ser capaz de manejar los siguientes mensajes:

-   `mcast, Msg`: un mensaje es de su propio master o de algún par del
    nodo. Se debe hacer multicast un mensaje `mcast, Msg` a todos sus
    pares y el mensaje `Msg` enviado a la capa de aplicación.

-   `join, Wrk, Peer`: un mensaje de un par o el master, que es un
    pedido de un nodo para unirse al grupo. El mensaje contienen tanto
    el identificador del proceso de la capa de aplicación, `Wrk`, como
    el identificador del proceso de su proceso de grupo.

El estado de un líder es implementado de la siguiente función. Usamos
una función `bcast/3` que va a enviar un mensaje a cada uno de los
procesos en una lista.

```erlang
    leader(Id, Master, Slaves, Group) ->
        receive
            {mcast, Msg} ->
                bcast(Id, {msg, Msg}, Slaves),
                Master ! Msg,
                leader(Id, Master, Slaves, Group);
            {join, Wrk, Peer} ->
                Slaves2 = lists:append(Slaves, [Peer]),
                Group2 = lists:append(Group, [Wrk]),
                bcast(Id, {view, [self()|Slaves2], Group2}, Slaves2),
                Master ! {view, Group2},
                leader(Id, Master, Slaves2, Group2);
            stop -> ok
        end.

```

Notar que agregamos un nuevo nodo al final de la lista de pares. Esto es
importante, queremos que un nuevo nodo sea el último en ver un mensaje
de las vista que enviamos. Veremos más sobre esto más adelante cuando
veamos nodos que fallan.

Un Esclavo
----------

Un esclavo tiene una tarea aún más fácil, no va tomar ninguna decisión
complicada. Solo va a reenviar mensajes del master al líder y viceversa.
El estado de un esclavo es exactamente el estado del líder con la sola
excepción que los esclavos mantienen registro explícito del líder.

Los mensajes del master son los siguiente:

-   `mcast, Msg`: un pedido de su master para hacer multicast de un
    mensaje, el mensaje es reenviado al líder.

-   `join, Wrk, Peer`: un request de un master para permitir que un
    nuevo nodo se una al grupo, el mensaje es reenviado al líder.

-   `msg, Msg`: un mensaje que se hizo multicast desde un líder. Un
    mensaje `Msg` es enviado al master.

-   `view, Peers, Group`: una vista que se hizo multicast desde el
    líder. La vista es entregada al proceso master.

Esta es la implementación del esclavo:

```erlang
    slave(Id, Master, Leader, Slaves, Group) ->
        receive
            {mcast, Msg} ->
                Leader ! {mcast, Msg},
                slave(Id, Master, Leader, Slaves, Group);
            {join, Wrk, Peer} ->
                Leader ! {join, Wrk, Peer},
                slave(Id, Master, Leader, Slaves, Group);
            {msg, Msg} ->
                Master ! Msg,
                slave(Id, Master, Leader, Slaves, Group);
            {view, [Leader|Slaves2], Group2} ->
                Master ! {view, Group2}
                slave(Id, Master, Leader, Slaves2, Group2);
            stop ->
                ok
        end.

```

Dado que no vamos a lidiar con fallas no hay transición de esclavo a
líder. Vamos a agregar esto luego pero primero vamos a tratar de tener
algo andando.

Inicialización
--------------

Inicializar un proceso que es el primer nodo en un grupo es simple. Lo
único que necesitamos hacer es darle una lista vacía de pares y hacerles
saber que su master es es el único nodo del grupo. Dado que es el único
nodo en el grupo por supuesto va a ser el líder del grupo.

```erlang
    start(Id) ->
        Self = self(),
        {ok, spawn_link(fun()-> init(Id, Self) end)}.

    init(Id, Master) ->
        leader(Id, Master, [], [Master]).

```

Iniciar un nodo que debe unirse a un grupo existente es un poco más
complicado. Necesitamos enviar un mensaje `join, Master, self()` a un
nodo en el grupo y esperar la invitación. La invitación es entregada
como un mensaje de una vista conteniendo todo lo que conocemos. El
estado inicial es por supuesto como esclavo.

```erlang
    start(Id, Grp) ->
        Self = self(),
        {ok, spawn_link(fun()-> init(Id, Grp, Self) end)}.

    init(Id, Grp, Master) ->
        Self = self(),
        Grp ! {join, Master, Self},
        receive
            {view, [Leader|Slaves], Group} ->
                Master ! {view, Group},
                slave(Id, Master, Leader, Slaves, Group)
        end.

```

El Proceso Aplicación
---------------------

Para hacer alguna prueba creamos un worker que use una GUI para
describir su estado. Asegurarse de que podemos crear un grupo y agregar
algunos pares.

Manejando Errores
=================

Vamos a construir nuestro esquema de tolerancia a fallas en forma
gradual. Primero vamos a asegurarnos de detectar las fallas, luego nos
vamos a asegurar que un líder es electo y por último asegurarnos que la
capa preserva la propiedad de multicast atómico. Mantener `gms1` como
referencia y llamemos al módulo adaptado `gms2`.

Detectores de Fallas
--------------------

Vamos a usar el soporte de Erlang para detectar y reportar que los
procesos murieron. Un proceso puede monitorear otro nodo y si ese nodo
muere va a recibir un mensaje. Por ahora vamos a asumir que los
monitores son perfectos, esto es, eventualmente va a a recibir el
reporte de falla de de un nodo y nunca van a reportar la muerte de un
nodo que no lo haya hecho.

También vamos a asumir que los mensajes que informan a un proceso de la
muerte de otro es el último mensaje que vemos de un nodo. El mensaje
entonces es recibido en orden FIFO como cualquier mensaje común.

La pregunta que primero debemos responder es, quién debe monitorear a
quién? En nuestra arquitectura no necesitamos reportar nuevas vistas
cuando un esclavo muere y no hay nada que prevenga que un esclavo muerto
sea parte de una vista, así que vamos a mantener las cosas simples; el
único nodo que va a ser monitoreado es el líder. Un esclavo que detecta
que un líder murió cambiará a un estado de elección.

Esto se implementa agregando primero la llamada `erlang:monitor/2` en la
inicialización del esclavo:

```erlang
        erlang:monitor(process, Leader)

```

y una nueva cláusula al estado del esclavo:

```erlang
        {’DOWN’, _Ref, process, Leader, _Reason} ->
            election(Id, Master, Slaves, Group);

```

En el estado de elección el proceso va a seleccionar el primer nodo de
su lista de pares y seleccionarlo como líder. Podría pasar por supuesto
que el propio proceso sea el primer nodo y en tal caso convertirse en
líder del grupo.

```erlang
    election(Id, Master, Slaves, [_|Group]) ->
        Self = self(),
        case Slaves of
            [Self|Rest] ->
                bcast(Id, {view, Slaves, Group}, Rest),
                Master ! {view, Group},
                leader(Id, Master, Rest, Group);
            [Leader|Rest] ->
                erlang:monitor(process, Leader),
                slave(Id, Master, Leader, Rest, Group)
        end.

```

Algo que debemos prestar atención es que debemos hacer si, como esclavo,
recibimos un *mensaje de vista* de un nuevo líder antes que hayamos
detectado que el viejo líder murió. Deberíamos negarnos a manejar
mensajes de vista a menos que hayamos visto un *mensaje Down* del líder?
o simplemente deberíamos aceptar el mensaje del nuevo líder e ignorar el
*mensaje Down*?

Dado que el líder puede morir podría ser que un nodo que se quiere unir
nunca reciba una respuesta. El mensaje puede ser reenviado a un líder
muerto y el nodo queriendo unirse nunca va a ser informado del hecho que
su pedido se perdió. Para esto simplemente agregamos un timeout cuando
esperamos una invitación para unirnos al grupo.

```erlang
        after ?timeout ->
            Master ! {error, "no reply from leader"}

```

Ahora podemos agregar nodos nuevos al sistema y sobrevivir aún cuando
los nodos mueren. No fue tan difícil, no? Hacer algunas pruebas para ver
que esto funciona antes de que podamos entregar el producto.

Mensajes Perdidos
-----------------

Parece que es demasiado fácil la solución y desafortunadamente lo es.
Para mostrar que no funciona podemos cambiar la función `bcast/3` e
introducir caídas aleatorias. Definimos una constante `arrghh` que
define el riesgo de caída. Un valor de `100` significa que el proceso va
a caerse en promedio una vez cada 100 intentos. La definición de
`bcast/3` ahora se ve de la siguiente forma:

```erlang
    bcast(Id, Msg, Nodes) ->
        lists:foreach(fun(Node) -> Node ! Msg, crash(Id) end, Nodes).
    crash(Id) ->
        case random:uniform(?arghh) of
            ?arghh ->
                io:format("leader ~w: crash~n", [Id]),
                exit(no_luck);
            _ -> ok
        end.

```

Correr algunas pruebas y ver si tenemos estado en los nodos que quedan
de-sincronizados. Qué está sucediendo?

Multicast Confiable
-------------------

Para remediar el problema podemos remplazar el multicaster básico con
uno confiable. Un proceso va a reenviar todos los mensajes antes de
pasarlos a una capa superior. Usar un multicaster confiable básico
podría ser muy costoso, vamos a intentar una solución más inteligente.

Asumamos que mantenemos una copia del último mensaje que vimos de un
líder. Si detectamos la muerte del líder puede que haya sucedido durante
el procedimiento de multicast básico y algunos nodos no hayan visto el
mensaje. Vamos a asumir algo que vamos a discutir más adelante:

-   Los mensajes son confiables entonces,

-   si un líder envía un mensaje a A y entonces B, y B recibe el
    mensaje, entonces A va a recibir el mensaje.

El líder envía mensajes a los pares en el orden que aparecen en la lista
de pares. Si alguno recibe un mensaje entonces el primer par en la lista
recibe el mensaje. Esto significa que solo el siguiente al líder
necesita reenviar el mensaje.

Esto por supuesto va a introducir la posibilidad de que se dupliquen
mensajes recibidos. Para detectar esto vamos a numerar todos los
mensajes y solo entregar nuevos mensajes a la capa de aplicación.

Vamos a ver los cambios que necesitamos para hacer el nuevo módulo
`gsm3` que implementa estos cambios.

-   `slave(Id, Master, Leader, N, Last, Slaves, Group)`: el
    procedimiento del esclavo es extendido con 2 argumentos: `N` y
    `Last`. `N` es el número de secuencia esperado del siguiente mensaje
    y `Last` es una copia del último mensaje (un mensaje común o de
    vista) recibido del líder.

-   `election(Id, Master, N, Last, Slaves, Group)`: el mecanismo de
    elección se extiende con los mismos 2 argumentos.

-   `leader(Id, Master, N, Slaves)`: el líder es extendido con el
    argumento `N`, el número de secuencia del siguiente mensaje (común o
    vista) a ser enviado.

Los mensajes también hay que cambiarlos para que ahora tengan el número
de secuencia.

-   `msg, N, Msg`: un mensaje común con el número de secuencia.

-   `view, N, Peers, Group`: una vista con un número de secuencia.

También debemos agregar cláusulas al esclavo para aceptar e ignorar
mensajes duplicados. Si no removemos estos mensajes de la cola de
mensajes se van a ir encolando y generar problemas.

Cuando descartamos mensajes solo descartamos aquellos que ya hemos
visto, esto es, mensajes con número de secuencia menor que `N`. Hacemos
esto usando la construcción `when`. Por ejemplo:

```erlang
    {msg, I, _} when I < N ->
        slave(Id, Master, Leader, N, Last, Slaves, Group);

```

Nos estaremos preguntando cómo un mensaje podría llegar temprano, pero
hay una pequeña ventana donde esto podría efectivamente pasar.

La parte crítica es en el procedimiento de elección cuando el líder
elegido va a reenviar el último mensaje recibido a todos su pares en el
grupo. Con suerte esto va a ser suficiente para mantener los esclavos
sincronizados.

```erlang
      bcast(Id, Last, Rest),

```

Esto completa la transición y `gms3` debería estar listo para
producción.

Algunos Experimentos
--------------------

Ejecutar algunos experimentos y crear un grupo grande que se extienda a
varias computadoras. Hay sin embargo un tema que debemos mencionar an
esto es que nuestra implementación no funciona. Bueno, algunas cosas
funcionan dependiendo de que garantice el entorno Erlang y cuan fuerte
son nuestros requerimientos.

Qué Podría Salir Mal?
=====================

Lo primero que debemos darnos cuentas es que garantiza Erlang en el
envío de mensajes. La especificación garantiza solo que los mensajes son
entregados en orden FIFO, no que necesariamente lleguen. Nosotros
construimos nuestro sistema basados en la entrega confiable de mensajes,
algo que no está garantizado.

Cómo deberíamos cambiar nuestra implementación para manejar la
posibilidad de que se pierda un mensaje? Cuál sería el impacto de esto
en la performance?

La segunda razón por la cual las cosas no funcionan es que nos basamos
que la detección de fallas de Erlang es perfecto, esto es, que nunca va
a sospechar que un nodo correcto ha muerto. Es esto siempre así? Podemos
adaptar el sistema para que se comporte correctamente si hay progreso,
aún cuando podría no siempre tener progreso?

La tercer razón de por qué las cosas no funcionan es que podemos tener
una situación donde un nodo incorrecto entrega un mensaje que no fue
entregado por ningún nodo correcto. Esto puede suceder aún si tenemos
envío confiable y detección de fallas perfecta. Cómo podría pasar esto y
cuán seguido Cómo sería una solución?

[^1]: Adaptado al español del material original de Johan Montelius
    (<https://people.kth.se/~johanmon/dse.html>)
